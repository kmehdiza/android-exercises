fun main(array: Array<String>) {
    displayMax()
    displayMin()
    compareNumber(10)
    println(increaseScore(85.0))
    displayTimeWithWords(70)
    displayCurrentTemp(38)
}

fun displayMax() {
    val byteMax: Byte = Byte.MAX_VALUE
    val intMax: Int = Int.MAX_VALUE
    val doubleMax: Double = Double.MAX_VALUE
    val floatMax: Float = Float.MAX_VALUE
    val longMax: Long = Long.MAX_VALUE
    println("byteMax: " + byteMax)
    println("intMax: " + intMax)
    println("doubleMax: " + doubleMax)
    println("floatMax: " + floatMax)
    println("longMax: " + longMax)
}


fun displayMin() {
    val byteMin: Byte = Byte.MIN_VALUE
    val intMin: Int = Int.MIN_VALUE
    val doubleMin: Double = Double.MIN_VALUE
    val floatMin: Float = Float.MIN_VALUE
    val longMin: Long = Long.MIN_VALUE
    println("byteMin: " + byteMin)
    println("intMin: " + intMin)
    println("doubleMin: " + doubleMin)
    println("floatMin: " + floatMin)
    println("longMin: " + longMin)
}

fun compareNumber(y: Int) {
    var x: Int? = null
    if (y > 0) {
        x = 0
    }
    print(x)
}

fun increaseScore(score: Double): Double {
    var scores = score
    if (scores > 80 && scores < 90) {
        scores += 5
    }
    return scores
}

fun displayTimeWithWords(time: Int) {
    if (time > 6 && time < 18) {
        println("Good Day!")
    } else {
        println("Good Night!")
    }
}

fun displayCurrentTemp(number: Int){
   var result =  when(number) {
        -40 -> "down Freezing"
        in -40..-20 -> "Very Cold"
        in -20..0 -> "Cold"
        in 0..20 -> "Mild"
       in 20..40 -> "Warm"
       else -> "Dangerous"
    }
    println(result)
}